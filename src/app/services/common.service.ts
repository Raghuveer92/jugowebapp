import { Injectable } from '@angular/core';
import { HttpRequest } from '../models/HttpRequest';
import { RequestOptions, Headers, Http } from '@angular/http';
// import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()

export class CommonService {

    constructor(private http: Http) {
    }

    callHttpReq(req: HttpRequest) {
        const options = new RequestOptions({ headers: req.headers });
        if (req.method === 'GET') {
            return this.http.get(req.url, options).map(res => res.json());
        } else if (req.method === 'POST') {
            return this.http.post(req.url, req.params, options).map(res => res.json());
        }
    }
}

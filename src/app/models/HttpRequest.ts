import { Headers } from '@angular/http';
import { ClassType } from 'class-transformer/ClassTransformer';
import { BaseResponse } from './BaseResponseModel';

export class HttpRequest {

    url: string;
    params: any;
    method: string;
    taskcode: number;
    taskCode: number;
    headers: Headers;
    classTypeValue: ClassType<any> = BaseResponse;
    isArrayResponse: false;

    constructor(url: string) {
        this.url = url;
        this.method = 'GET';
        this.headers = new Headers();
        // this.addDefaultHeaders();
    }
    addDefaultHeaders() {
        this.headers.append('content-Type', 'application/json');
        this.headers.append('authToken', '12345678');
        // this.headers.append('authToken', localStorage.getItem('token'));
        this.headers.append('roleType', '4');
        // this.headers.append('roleType', localStorage.getItem('roleType'));
    }
    removeDefaultHeaders() {
        this.headers.delete('content-Type');
        this.headers.delete('authToken');
        this.headers.delete('roleType');
    }
    removeHeaders(key: string) {
        this.headers.delete(key);
    }
    addHeaders(key: string, value: string) {
        this.headers.append(key, value);
    }
    setPostMethod() {
        this.method = 'POST';
    }
    setPatchMethod() {
        this.method = 'PATCH';
    }
    setPutMethod() {
        this.method = 'PUT';
    }
}

export class HttpGenericRequest<T> extends HttpRequest {
    classType: ClassType<T>;
    constructor(url: string) {
        super(url);
    }
}

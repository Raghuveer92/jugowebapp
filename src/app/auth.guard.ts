import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { KEYS } from './globals';

@Injectable()

export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate() {
        if (localStorage.getItem(KEYS.LOGIN_DATA_KEY)) {
            return true;
        }
        this.router.navigate(['./login']);
        return false;
    }
}

import { HttpRequest, HttpGenericRequest } from './models/HttpRequest';
import { TaskCode } from './globals';
import * as global from './globals';
import { classToPlain, plainToClass } from 'class-transformer';
import { ClassType } from 'class-transformer/ClassTransformer';

// import { DistributorDetailsModel } from './models/DistibutorDetailsModel';

export class ApiGenerator {

    static getSuggestedDistributorList(queryString) {
        // const httpreq = new HttpRequest(global.GET_SUGGESTED_DISTRIBUTOR_LIST_API.concat(queryString));
        // httpreq.classTypeValue = DistributorDetailsModel;
        // httpreq.taskCode = TaskCode.GET_DISTRIBUTORS_LIST;
        // return httpreq;
    }

    //write methods here
}


export class JsonParser {
    static parseJson<T>(response: any, type: ClassType<T>): T {
        const parsedResponse = plainToClass(type, response as object);
        return parsedResponse;
    }

    static parseJsonString(response: any, type: ClassType<any>): any {
        const parsedResponse = plainToClass(type, response as object);
        return parsedResponse;
    }

    static parseJsonArray(response: any, type: ClassType<any>): any {
        const parsedResponse = plainToClass(type, response);
        return parsedResponse;
    }
}

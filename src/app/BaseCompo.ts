import { OnInit } from '@angular/core';
import { CommonService } from './services/common.service';
import { TaskCode } from './globals';
import { BaseResponse } from './models/BaseResponseModel';
// import { Logger } from './utils/Logger';
import { HttpRequest, HttpGenericRequest } from './models/HttpRequest';
import { DownloadManager } from './DownloadManager';

export class BaseComponent implements OnInit {
    constructor(protected commonService: CommonService) {  }

    ngOnInit() {
    }

    downloadData(req: HttpRequest) {
        const manager = new DownloadManager(this, this.commonService);
        manager.downloadData(req);
    }

    onPreExecute(taskCode: TaskCode) {
        console.log('on preExecute of basecomponent');
        // showLoader()
    }

    onErrorReceived(taskCode: TaskCode) {
        console.log('on error recevied of base compo');
        // stopLoader()
    }

    logOut() { }

    onResponseReceived(taskCode: TaskCode, response: any) {
        console.log('onrespponse recevied of base compo');
        // Logger.log(response);
        if (response instanceof BaseResponse) {
            return !response.error;
        }
        return true;
        // stopLoader()
    }
}

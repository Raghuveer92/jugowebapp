import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonService } from '../services/common.service';
import {FormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { UserRoutingModule } from './user-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    UserRoutingModule,FormsModule
  ],
  providers: [ CommonService],
  declarations: []
})
export class UserModule { }
